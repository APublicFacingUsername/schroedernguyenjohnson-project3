This is a project for the Compilers 1 course at Augsburg College, Fall 2016. The project members are Charlie Schroeder, Uyen Nguyen, and Matthew Johnson.


The project is a webpage that keeps track of your contacts and their information. 
On the left side of the page, you can view a list of all your contacts, and select one to view their details. 
All the information on the selected contact will appear on the right side of the screen. 
An edit mode is available if you need to update an existing contact, or create a new one.
Any changes made using the edit mode will not persist across loads of the webpage; this webpage is merely an instructional tool.


Changelog:


1611?? - Implemented the CSS and HTML layout


1611?? - Implemented the JavaScript functions for adding contacts, edited HTML to change contacts.


161118 - Implemented the JavaScript function for editing contacts and saving them to the current session.