var addButtonClick;
var modal;
var span;
var submitButtonClick;
var numberOfContacts;
var contactArray = [];
var currentContactNum;

function startup()
{
//setting up the page elements that need to be referenced
addButtonClick = document.getElementById("addButton");
modal = document.getElementById("newContactModal");
span = document.getElementById("closeModal");
submitButtonClick = document.getElementById("submitButton");
editButtonClick = document.getElementById("editButton");

addButtonClick.addEventListener("click", openModal, false);
submitButtonClick.addEventListener("click", createNewContact, false);
editButtonClick.addEventListener("click", setEditable, false);

contactArray.push(new contact("Matt Johnson", "867-5309", "867-5309", "email@augsburg.edu", "123 3rd Ave NW"));
contactArray.push(new contact("Uyen Nguyen", "000-000-0000", "111-111-1111", "augsburgEmail@augsburg.edu", "321 99th Str"));
contactArray.push(new contact("Charlie Schroeder", "612-275-3316", "555-123-4567", "cschroeder08@hamline.edu", "123 House Street"));
numberOfContacts = 3;

span.onclick = function() 
{
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) 
{
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

}

//This function is a prototype for the contact object. It takes in strings.

function contact(fullName, mobilePhoneNumber, homePhoneNumber, email, streetAddress)
{
    this.contactFullName = fullName;
    this.contactMobilePhoneNumber = mobilePhoneNumber;
    this.contactHomePhoneNumber = homePhoneNumber;
    this.contactEmail = email;
    this.contactStreetAddress = streetAddress;
}

//This function updates the information being displayed by the HTML.

function switchContact(whoToSwitchTo)
{
	contactName.innerHTML = whoToSwitchTo.contactFullName;
    mobile.innerHTML = whoToSwitchTo.contactMobilePhoneNumber;
    home.innerHTML = whoToSwitchTo.contactHomePhoneNumber;
    address.innerHTML = whoToSwitchTo.contactStreetAddress;
    email.innerHTML = whoToSwitchTo.contactEmail;
	editButton.innerHTML = "Edit";
}

//This function handles the user clicking on the HTML contactList.

function clickHandler(indexValue)
{       
    for (var i =0; i < contactArray.length; i++)
    {  
        if(i == indexValue)
        {
             switchContact(contactArray[i]);
			 currentContactNum = i;
        }        
    }        
}                    

function createNewContact()
{
    //this first section adds all the relevant info to the HTML
    var newFullName = document.getElementsByName("newFullName")[0].value;
	if(newFullName != ""){
		var newMobilePhone = document.getElementsByName("newMobilePhone")[0].value;
		var newHomePhone = document.getElementsByName("newHomePhone")[0].value;
		var newEmail = document.getElementsByName("newEmail")[0].value;
		var newStreetAddress = document.getElementsByName("newStreetAddress")[0].value;
		//this section creates a new item in the li list under the Ul id contactUlList. It then appends the text, and function call.
		var node = document.createElement("LI");
		var addedContact = document.createTextNode(newFullName);
		node.appendChild(addedContact);
		var numberOfContactsCast = String(numberOfContacts);
		node.onclick = function () {clickHandler(numberOfContactsCast);};
		node.id = "c" + numberOfContactsCast;
		numberOfContacts++;
		document.getElementById("contactUlList").appendChild(node);
		//this pushes a new contact object into the array
		contactArray.push(new contact(newFullName, newMobilePhone, newHomePhone, newEmail, newStreetAddress));
		//this switches the current view to the newly created contact and closes the modal.
		switchContact(contactArray[contactArray.length -1]);
		modal.style.display = "none";
        //reset placeholders
        document.getElementsByName("newFullName")[0].value = null;
        document.getElementsByName("newMobilePhone")[0].value = null;
        document.getElementsByName("newHomePhone")[0].value = null;
        document.getElementsByName("newEmail")[0].value = null;
        document.getElementsByName("newStreetAddress")[0].value = null;
	}
}

function openModal()
{
    modal.style.display = "block";
}

function setEditable(){
	//Do something based on the html contained in the button named editButton, aka the displayed name of the button
	if(editButton.innerHTML == "Edit"){
		//Change the edit button to read "save"
		editButton.innerHTML = "Save";
		//Change the data fields to be text boxes with same values and unique ids
		contactName.innerHTML = "<input type=\"text\" value=\"" + contactName.innerHTML + "\" id=\"editingName\">";
		mobile.innerHTML = "<input type=\"text\" value=\"" + mobile.innerHTML + "\" id=\"editingMobile\">";
		home.innerHTML = "<input type=\"text\" value=\"" + home.innerHTML + "\" id=\"editingHome\">";
		address.innerHTML = "<input type=\"text\" value=\"" + address.innerHTML + "\" id=\"editingAddress\">";
		email.innerHTML = "<input type=\"text\" value=\"" + email.innerHTML + "\" id=\"editingEmail\">";
	} else if(editButton.innerHTML == "Save"){
		//Change the edit button to read "edit"
		editButton.innerHTML = "Edit";
		//Change the values in our array based on what was contained in the text boxes
		contactArray[currentContactNum].contactFullName = editingName.value;
		contactArray[currentContactNum].contactMobilePhoneNumber = editingMobile.value;
		contactArray[currentContactNum].contactHomePhoneNumber = editingHome.value;
		contactArray[currentContactNum].contactStreetAddress = editingAddress.value;
		contactArray[currentContactNum].contactEmail = editingEmail.value;
		//Change the name displayed in the name list
		var listedName = document.getElementById("c" + String(currentContactNum));
		sName = editingName.value.split(" ");
		listedName.innerHTML = sName[0];
		//Change the data fields back to simple strings, no longer text boxes
		contactName.innerHTML = editingName.value;
		mobile.innerHTML = editingMobile.value;
		home.innerHTML = editingHome.value;
		address.innerHTML = editingAddress.value;
		email.innerHTML = editingEmail.value;
	}
} 